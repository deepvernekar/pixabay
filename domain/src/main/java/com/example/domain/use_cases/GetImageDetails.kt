package com.example.domain.use_cases


import com.example.common.Resource
import com.example.domain.model.Images
import com.example.domain.repository.ImageRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetImageDetails @Inject constructor(val imageRepository: ImageRepository) {

    suspend operator fun invoke(id:Int): Flow<Resource<Images>> = flow {
       emit(Resource.Loading(message = null))
        when(val resource = imageRepository.getImageDetails(id)){
            is Resource.Success -> emit(Resource.Success(data = resource.data!!))
            is Resource.Error -> emit(Resource.Error<Images>(message = resource.message))
        }
    }
}