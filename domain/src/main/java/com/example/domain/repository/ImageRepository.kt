package com.example.domain.repository

import androidx.paging.PagingData
import com.example.common.Resource
import com.example.domain.model.Image
import com.example.domain.model.Images
import kotlinx.coroutines.flow.Flow

interface ImageRepository {
    suspend fun searchImages(key:String):Flow<PagingData<Image>>
    suspend fun getImageDetails(id:Int):Resource<Images>
}