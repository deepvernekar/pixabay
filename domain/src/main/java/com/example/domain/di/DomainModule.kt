package com.example.domain.di

import com.example.domain.repository.ImageRepository
import com.example.domain.use_cases.GetImageDetails
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object DomainModule {

    @Provides
    fun provideGetImageDetailsUseCase(imageRepository: ImageRepository):GetImageDetails{
        return GetImageDetails(imageRepository)
    }
}