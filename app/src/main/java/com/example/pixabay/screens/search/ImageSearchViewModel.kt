package com.example.pixabay.screens.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.domain.model.Image
import com.example.domain.repository.ImageRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ImageSearchViewModel @Inject constructor(
    private val imageRepository: ImageRepository
) : ViewModel() {

    private var searchJob:Job? = null
    private val _searchedImages = MutableLiveData<PagingData<Image>>(PagingData.empty())
    val searchedImages = _searchedImages

    fun searchImages(key:String){
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            delay(500)
            if(key.isNotBlank() && key.length > 3) {
                imageRepository.searchImages(key = key).cachedIn(viewModelScope).collect { pagindData ->
                    _searchedImages.value = pagindData
                }
            }
        }
    }

    companion object{
        private const val TAG = "ImageSearchViewModel"
    }
}