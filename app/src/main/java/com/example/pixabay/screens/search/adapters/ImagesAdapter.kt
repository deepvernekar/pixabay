package com.example.pixabay.screens.search.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.model.Image
import com.example.pixabay.BR
import com.example.pixabay.R
import com.example.pixabay.databinding.ItemViewImageBinding

class ImagesAdapter(
    var imageList:List<Image>,
    val listener:OnItemClickListener) : RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {

    interface OnItemClickListener{
        fun onItemClicked(image:Image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding:ItemViewImageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_view_image,parent,
            false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(imageList[position])
        holder.itemView.setOnClickListener {
            listener.onItemClicked(imageList[position])
        }
    }

    override fun getItemCount(): Int = imageList.size

    fun update(imageList:List<Image>){
        this.imageList = imageList
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val itemViewImageBinding: ItemViewImageBinding) : RecyclerView.ViewHolder(itemViewImageBinding.root){
        fun bind(item:Any){
            itemViewImageBinding.setVariable(BR.image,item)
            itemViewImageBinding.executePendingBindings()
        }
    }
}