package com.example.pixabay.screens.details

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.pixabay.BR
import com.example.pixabay.databinding.FragmentImageDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImageDetailsFragment : Fragment() {
    val viewModel:ImageDetailsViewModel by viewModels()
    lateinit var binding:FragmentImageDetailsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentImageDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.detailScreenState.observe(this){
            
            if(it.isLoading){
                Log.e(TAG, "onViewCreated: ", )
            }
            
            it.data?.let { images ->
                binding.setVariable(BR.image,images.hits[0])
                binding.executePendingBindings()
            }
            
            if(it.error.isNotBlank()){
                Log.e(TAG, "onViewCreated: ", )
            }
        }
    }
    
    companion object{
        private const val TAG = "ImageDetailsFragment"
    }

}