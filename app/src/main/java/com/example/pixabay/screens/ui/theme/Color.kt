package com.example.pixabay.screens.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0x121212)
val Purple500 = Color(0x000000)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)