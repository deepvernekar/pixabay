package com.example.pixabay.screens.search

import com.example.domain.model.Images

data class SearchScreenState(
    val isLoading:Boolean = false,
    val data:Images? = null,
    val error:String = ""
)
