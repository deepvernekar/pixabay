package com.example.pixabay.screens.search

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.common.Constant.DEFAULT_SEARCH_QUERY
import com.example.common.Constant.IMAGE_ID
import com.example.domain.model.Image
import com.example.pixabay.R
import com.example.pixabay.databinding.FragmentImageSearchBinding
import com.example.pixabay.screens.search.adapters.ImagesAdapter
import com.example.pixabay.screens.search.adapters.ImagesPagingDataAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ImageSearchFragment : Fragment(),ImagesAdapter.OnItemClickListener {

    val viewModel:ImageSearchViewModel by viewModels()
    private lateinit var binding:FragmentImageSearchBinding
    private val imageAdapter by lazy { ImagesPagingDataAdapter(this) }
    lateinit var selectedImage:Image

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentImageSearchBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.searchedImages.observe(this){ response ->
            lifecycleScope.launch {
                imageAdapter.submitData(response)
            }
        }

        binding.imageList.apply {
            layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false)
            adapter = imageAdapter
            addItemDecoration(DividerItemDecoration(context,LinearLayoutManager.VERTICAL))
        }

        binding.searchField.apply {
            setOnQueryTextListener(QueryTextChangeListener())
            setQuery(DEFAULT_SEARCH_QUERY,true)
        }
    }

    inner class QueryTextChangeListener : SearchView.OnQueryTextListener{
        override fun onQueryTextSubmit(p0: String?): Boolean = true
        override fun onQueryTextChange(value: String): Boolean {
            viewModel.searchImages(value)
            return true
        }
    }

    override fun onItemClicked(image: Image) {
        selectedImage = image
        showDialog()
    }

    private fun showDialog(){
        val builder = AlertDialog.Builder(context!!);
        builder.setMessage(getString(R.string.dialog_title))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.positive_button_label)) { _, _ ->
                val bundle = bundleOf(IMAGE_ID to selectedImage.id)
                findNavController().navigate(R.id.action_imageSearchFragment2_to_imageDetailsFragment,bundle)
            }
            .setNegativeButton(getString(R.string.negative_button_label)){ dialog, _ -> dialog.cancel()}

        val alert = builder.create()
        alert.show()
    }
}