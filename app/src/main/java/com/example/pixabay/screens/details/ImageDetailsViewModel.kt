package com.example.pixabay.screens.details

import androidx.lifecycle.*
import com.example.common.Constant.IMAGE_ID
import com.example.common.Resource
import com.example.domain.use_cases.GetImageDetails
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ImageDetailsViewModel @Inject constructor(
    val getImageDetailsUseCase: GetImageDetails,
    private val savedStateHandle: SavedStateHandle) : ViewModel() {

    private val _detailScreenState = MutableLiveData<DetailScreenState>()
    val detailScreenState:LiveData<DetailScreenState> = _detailScreenState

    init {
        savedStateHandle.get<Int>(IMAGE_ID)?.let {
            getImageDetails(it)
        }
    }

    private fun getImageDetails(id:Int){
        viewModelScope.launch {
            getImageDetailsUseCase(id).onEach {
                _detailScreenState.value = when(it){
                    is Resource.Loading ->  DetailScreenState(isLoading = true)
                    is Resource.Success ->  DetailScreenState(data = it.data)
                    is Resource.Error ->    DetailScreenState(error = it.message.toString())
                }
            }.launchIn(viewModelScope)
        }
    }

    companion object{
        private const val TAG = "ImageDetailsViewModel"
    }
}