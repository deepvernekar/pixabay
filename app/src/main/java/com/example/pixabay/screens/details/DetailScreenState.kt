package com.example.pixabay.screens.details

import com.example.domain.model.Images

data class DetailScreenState(
    var isLoading:Boolean = false,
    var data:Images? = null,
    var error:String = ""
)