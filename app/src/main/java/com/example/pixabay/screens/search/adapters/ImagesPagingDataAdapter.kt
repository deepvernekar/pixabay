package com.example.pixabay.screens.search.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.model.Image
import com.example.pixabay.BR
import com.example.pixabay.R
import com.example.pixabay.databinding.ItemViewImageBinding

class ImagesPagingDataAdapter(val listener: ImagesAdapter.OnItemClickListener) :
    PagingDataAdapter<Image,ImagesPagingDataAdapter.ViewHolder>(IMAGE_COMPARATOR) {

    interface OnItemClickListener{
        fun onItemClicked(image:Image)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
        holder.itemView.setOnClickListener {
            listener.onItemClicked(getItem(position)!!)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding:ItemViewImageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_view_image,parent,
            false)
        return ViewHolder(binding)
    }

    inner class ViewHolder(private val itemViewImageBinding: ItemViewImageBinding) : RecyclerView.ViewHolder(itemViewImageBinding.root){
        fun bind(item:Image){
            itemViewImageBinding.setVariable(BR.image,item)
            itemViewImageBinding.executePendingBindings()
        }
    }

    companion object{
        private val IMAGE_COMPARATOR = object :DiffUtil.ItemCallback<Image>(){
            override fun areItemsTheSame(oldItem: Image, newItem: Image): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Image, newItem: Image): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}