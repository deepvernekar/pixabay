package com.example.pixabay.common

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.domain.model.Image


@BindingAdapter("image")
fun setImage(imageView:ImageView,image: Image?){
    if(image != null) {
        Glide.with(imageView.context)
            .load(image.previewURL)
            .centerCrop()
            .into(imageView)
    }
}

@BindingAdapter("largeimage")
fun setLargeImage(imageView:ImageView,image:Image?){
    if(image != null) {
        Glide.with(imageView.context)
            .load(image.largeImageURL)
            .centerCrop()
            .into(imageView)
    }
}
