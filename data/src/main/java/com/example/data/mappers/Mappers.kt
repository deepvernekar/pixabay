package com.example.data.mappers


import com.example.data.network.model.ImageDTO
import com.example.data.network.model.ImagesDTO
import com.example.domain.model.Image
import com.example.domain.model.Images

fun List<ImageDTO>.toDomain():List<Image>{
    return map{
        Image(
            collections = it.collections?:0,
            comments = it.comments?:0,
            downloads = it.downloads?:0,
            id = it.id?:0,
            imageHeight = it.imageHeight?:0,
            imageSize = it.imageSize?:0,
            imageWidth = it.imageWidth?:0,
            largeImageURL = it.largeImageURL?:"",
            likes = it.likes?:0,
            pageURL = it.pageURL?:"",
            previewHeight = it.previewHeight?:0,
            previewURL = it.previewURL?:"",
            previewWidth = it.previewWidth?:0,
            tags = it.tags?:"",
            type = it.type?:"",
            user = it.user?:"",
            userId = it.userId?:0,
            userImageURL = it.userImageURL?:"",
            views = it.views?:0,
            webformatHeight = it.webformatHeight?:0,
            webformatURL = it.webformatURL?:"",
            webformatWidth = it.webformatWidth?:0
        )
    }
}

fun ImagesDTO.toDomain():Images{
    return Images(
        hits = this.hits?.toDomain()?: emptyList(),
        total = this.total?:0,
        totalHits = this.totalHits?:0
    )
}