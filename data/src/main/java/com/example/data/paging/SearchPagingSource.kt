package com.example.data.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.common.Constant.PAGE_SIZE
import com.example.data.mappers.toDomain
import com.example.data.network.ApiService
import com.example.data.network.model.ImageDTO
import com.example.data.network.model.ImagesDTO
import com.example.domain.model.Image
import java.lang.Exception

class SearchPagingSource(
    private val imageSource:ApiService,
    private val query:String
):PagingSource<Int,Image>() {

    override fun getRefreshKey(state: PagingState<Int, Image>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Image> {
        val currentPage = params.key ?: 1

        return try {
            val response = imageSource.searchImages(
                q = query,
                perPage = PAGE_SIZE,
                pageNumber = currentPage)

            val endOfPaginationReached = response.hits?.isEmpty()

            if (!response.hits.isNullOrEmpty()) {
                LoadResult.Page(
                    data = response.toDomain().hits,
                    prevKey = if (currentPage == 1) null else currentPage - 1,
                    nextKey = if (endOfPaginationReached!!) null else currentPage + 1
                )
            } else {
                LoadResult.Page(
                    data = emptyList(),
                    prevKey = null,
                    nextKey = null
                )
            }
        }catch (e:Exception){
            LoadResult.Error(e)
        }
    }
}