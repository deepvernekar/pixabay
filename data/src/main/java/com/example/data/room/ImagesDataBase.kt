package com.example.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.domain.model.Image

@Database(entities = [Image::class,ImageKey::class], version = 1, exportSchema = false)
abstract class ImagesDataBase : RoomDatabase() {

    companion object{
        fun getInstance(context:Context):ImagesDataBase{
            return Room.databaseBuilder(context,ImagesDataBase::class.java,"images").build()
        }
    }
    abstract fun getImageDAO():ImageDAO
}