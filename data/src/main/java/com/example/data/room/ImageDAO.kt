package com.example.data.room

import androidx.paging.PagingSource
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.domain.model.Image

interface ImageDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertImages(images:List<Image>)

    @Query(value="SELECT * FROM Image")
    fun getAllImages():PagingSource<Int,Image>

    @Query("DELETE FROM Image")
    suspend fun deleteAllImages()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertImageKeys(keys:List<ImageKey>)

    @Query("DELETE FROM ImageKey")
    suspend fun deleteAllImageKeys()

    @Query("SELECT * FROM ImageKey WHERE id=:id")
    suspend fun getAllImageKeys(id:Int):ImageKey
}