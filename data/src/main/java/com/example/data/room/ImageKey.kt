package com.example.data.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ImageKey(
    @PrimaryKey(autoGenerate = true)
    var id:Int,
    var prev:Int?,
    var next:Int?
)
