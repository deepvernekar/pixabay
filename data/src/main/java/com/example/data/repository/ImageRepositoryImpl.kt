package com.example.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.common.Constant.PAGE_SIZE
import com.example.common.Resource
import com.example.data.mappers.toDomain
import com.example.data.network.ApiService
import com.example.data.network.SafeApiCall
import com.example.data.paging.SearchPagingSource
import com.example.domain.model.Image
import com.example.domain.model.Images
import com.example.domain.repository.ImageRepository
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import javax.inject.Inject

class ImageRepositoryImpl @Inject constructor(private val imageService: ApiService) : ImageRepository {

    override suspend fun searchImages(key: String): Flow<PagingData<Image>> {
        return Pager(
            config = PagingConfig(pageSize = PAGE_SIZE),
            pagingSourceFactory = {SearchPagingSource(imageService,key)}
        ).flow
    }

    override suspend fun getImageDetails(id: Int): Resource<Images> {
        return when(val response = SafeApiCall.safeApiCall { imageService.getImageDetails(id=id) }){
            is Resource.Success -> Resource.Success(response.data?.toDomain()!!)
            is Resource.Error -> Resource.Error(response.message)
            is Resource.Loading -> Resource.Loading(null)
        }
    }
}