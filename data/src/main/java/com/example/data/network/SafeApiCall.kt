package com.example.data.network

import com.example.common.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

abstract class SafeApiCall {
    companion object{
        suspend inline fun <T> safeApiCall(crossinline body: suspend () -> T): Resource<T> {
            return try {
                val data = withContext(Dispatchers.IO) {
                    body()
                }
                Resource.Success(data)
            } catch (e: Exception) {
                Resource.Error(e.message)
            }
        }
    }
}