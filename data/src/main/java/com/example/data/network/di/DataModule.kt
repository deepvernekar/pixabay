package com.example.data.network.di

import android.content.Context
import com.example.common.Constant.BASE_URL
import com.example.data.BuildConfig
import com.example.data.network.ApiService
import com.example.data.repository.ImageRepositoryImpl
import com.example.data.room.ImageDAO
import com.example.data.room.ImagesDataBase
import com.example.domain.repository.ImageRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DataModule {

    @Provides
    @Singleton
    fun providesHttpLogging(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @Singleton
    fun providesHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder.addInterceptor(Interceptor{ chain ->
            val originalRequest = chain.request()
            val originalHttpUrl = originalRequest.url
            val url = originalHttpUrl.newBuilder()
                .addQueryParameter("key",BuildConfig.API_KEY)
                .build()

            val requestBuilder = originalRequest.newBuilder().url(url)
            var request = requestBuilder.build()
            chain.proceed(request)
        })

        return httpClientBuilder.addInterceptor(httpLoggingInterceptor)
            .build()
    }

    @Provides
    @Singleton
    fun providesRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun providesApiService(retrofit: Retrofit):ApiService{
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideImageRepository(apiService: ApiService):ImageRepository{
        return ImageRepositoryImpl(apiService)
    }

    @Provides
    @Singleton
    fun provideDataBase(@ApplicationContext context: Context):ImagesDataBase{
        return ImagesDataBase.getInstance(context)
    }

    @Provides
    @Singleton
    fun provideDataAccessObject(imagesDataBase: ImagesDataBase):ImageDAO{
        return imagesDataBase.getImageDAO()
    }
}