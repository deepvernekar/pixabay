package com.example.data.network.model


import com.google.gson.annotations.SerializedName

data class ImagesDTO(
    @SerializedName("hits")
    val hits: List<ImageDTO>?,
    @SerializedName("total")
    val total: Int?,
    @SerializedName("totalHits")
    val totalHits: Int?
)