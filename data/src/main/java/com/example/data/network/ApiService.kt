package com.example.data.network

import com.example.data.network.model.ImagesDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("api/")
    suspend fun searchImages(
        @Query("q") q:String,
        @Query("image_type") type:String = "photo",
        @Query("per_page") perPage:Int,
        @Query("page") pageNumber:Int
    ):ImagesDTO

    @GET("api/")
    suspend fun getImageDetails(
        @Query("id") id:Int
    ):ImagesDTO
}