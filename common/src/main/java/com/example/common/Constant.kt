package com.example.common

object Constant {
    const val BASE_URL = "https://pixabay.com/"
    const val PAGE_SIZE = 10
    const val DEFAULT_SEARCH_QUERY:String = "fruits"
    const val SPLASH_DELAY = 5000L
    const val IMAGE_ID = "id"
}